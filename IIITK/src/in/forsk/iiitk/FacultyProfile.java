package in.forsk.iiitk;

import in.forsk.iiitk.adapter.FacultyListAdapter;
import in.forsk.iiitk.wrapper.FacultyWrapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

public class FacultyProfile extends Activity {
	public final static String TAG = FacultyProfile.class.getSimpleName();
	private Context context;

	private ActionBar mActionBar;
	ImageView mMenuIcon;

	ListView mFacultyList;
	FacultyListAdapter mAdapter;

	// Collection Framework
	ArrayList<FacultyWrapper> mFacultyDataList = new ArrayList<FacultyWrapper>();

	Button mBackBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_faculty_profile);

		context = this;

		// Toast.makeText(context, TAG + " onCreate",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onCreate");

		// mActionBar = getActionBar();
		// mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		//
		// // Set CustomView in Action bar
		// LinearLayout action_bar_view = (LinearLayout)
		// getLayoutInflater().inflate(
		// R.layout.action_bar, null);
		// ActionBar.LayoutParams action_params = new ActionBar.LayoutParams(
		// ActionBar.LayoutParams.MATCH_PARENT,
		// ActionBar.LayoutParams.MATCH_PARENT,
		// Gravity.LEFT);
		// // mActionBar.setCustomView(R.layout.action_bar);
		// mMenuIcon = (ImageView) action_bar_view.findViewById(R.id.menuIcon);
		// mMenuIcon.setImageResource(R.drawable.back_icon);
		// mMenuIcon.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// onBackPressed();
		// }
		// });
		// mActionBar.setCustomView(action_bar_view, action_params);

		mFacultyList = (ListView) findViewById(R.id.facultyList);
		mBackBtn = (Button) findViewById(R.id.backBtn);

		// Local File Parsing
		try {
			String json_string = getStringFromRaw(context, R.raw.faculty_profile_code);

			ArrayList<FacultyWrapper> mFacultyDataList = pasreLocalFacultyFile(json_string);

			setFacultyListAdapter(mFacultyDataList);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// parseRemoteFacultyFile("http://iiitkota.forsklabs.in/faculty_profile.txt");

		mBackBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	public void printObject(FacultyWrapper obj) {
		// Operator Overloading
		Log.d(TAG, "First Name : " + obj.getFirst_name());
		Log.d(TAG, "Last Name : " + obj.getLast_name());
		Log.d(TAG, "Photo : " + obj.getPhoto());
		Log.d(TAG, "Department : " + obj.getDepartment());
		Log.d(TAG, "reserch_area : " + obj.getReserch_area());
		Log.d(TAG, "Phone : " + obj.getPhone());
		Log.d(TAG, "Email : " + obj.getEmail());

		for (String s : obj.getInterest_areas()) {
			Log.d(TAG, "Interest Area : " + s);
		}
	}

	private String getStringFromRaw(Context context, int resourceId) throws IOException {
		// Reading File from resource folder
		Resources r = context.getResources();
		InputStream is = r.openRawResource(resourceId);
		String statesText = convertStreamToString(is);
		is.close();

		Log.d(TAG, statesText);

		return statesText;
	}

	private String convertStreamToString(InputStream is) throws IOException {
		// Converting input stream into string
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = is.read();
		while (i != -1) {
			baos.write(i);
			i = is.read();
		}
		return baos.toString();
	}

	@Override
	protected void onStart() {
		super.onStart();

		// Toast.makeText(context, TAG + " onStart",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onStart");
	}

	@Override
	protected void onRestart() {
		super.onRestart();

		// Toast.makeText(context, TAG + " onRestart",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onRestart");
	}

	// Called if the activity get visible again and the user starts interacting
	// with the activity again. Used to initialize fields, register listeners,
	// bind to services, etc.
	@Override
	protected void onResume() {
		super.onResume();

		// Toast.makeText(context, TAG + " onResume",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onResume");
	}

	// Called once another activity gets into the foreground. Always called
	// before the activity is not visible anymore. Used to release resources or
	// save application data. For example you unregister listeners, intent
	// receivers, unbind from services or remove system service listeners.
	@Override
	protected void onPause() {
		super.onPause();

		// Toast.makeText(context, TAG + " onPause",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onPause");
	}

	// Called once the activity is no longer visible. Time or CPU intensive
	// shut-down operations, such as writing information to a database should be
	// down in the onStop() method. This method is guaranteed to be called as of
	// API 11.
	@Override
	protected void onStop() {
		super.onStop();

		// Toast.makeText(context, TAG + " onStop",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onStop");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		// Toast.makeText(context, TAG + " onDestroy",
		// //Toast.LENGTH_SHORT).show();
		notify(TAG + " onDestroy");
	}

	@SuppressLint("NewApi")
	private void notify(String methodName) {
		// String name = this.getClass().getName();
		// String[] strings = name.split("\\.");
		// Notification noti = new
		// Notification.Builder(this).setContentTitle(methodName + " " +
		// strings[strings.length -
		// 1]).setAutoCancel(true).setSmallIcon(R.drawable.ic_launcher)
		// .setContentText(name).build();
		// NotificationManager notificationManager = (NotificationManager)
		// getSystemService(NOTIFICATION_SERVICE);
		// notificationManager.notify((int) System.currentTimeMillis(), noti);
	}

	public void setFacultyListAdapter(ArrayList<FacultyWrapper> mFacultyDataList) {
		mAdapter = new FacultyListAdapter(context, mFacultyDataList);
		mFacultyList.setAdapter(mAdapter);
	}

	public ArrayList<FacultyWrapper> pasreLocalFacultyFile(String json_string) {

		ArrayList<FacultyWrapper> mFacultyDataList = new ArrayList<FacultyWrapper>();
		try {
			// Converting multipal json data (String) into Json array
			JSONArray facultyArray = new JSONArray(json_string);
			Log.d(TAG, facultyArray.toString());
			// Iterating json array into json objects
			for (int i = 0; facultyArray.length() > i; i++) {

				// Extracting json object from particular index of array
				JSONObject facultyJsonObject = facultyArray.getJSONObject(i);

				// Design patterns
				FacultyWrapper facultyObject = new FacultyWrapper(facultyJsonObject);

				printObject(facultyObject);

				mFacultyDataList.add(facultyObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mFacultyDataList;
	}

	public void parseRemoteFacultyFile(final String url) {
		new FacultyParseAsyncTask().execute(url);
	}

	// Params, Progress, Result
	class FacultyParseAsyncTask extends AsyncTask<String, Integer, String> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pd = new ProgressDialog(context);
			pd.setMessage("loading..");
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String url = params[0];
			InputStream is = openHttpConnection(url);
			String response = "";
			try {
				response = convertStreamToString(is);

				onProgressUpdate(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return response;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			ArrayList<FacultyWrapper> mFacultyDataList = pasreLocalFacultyFile(result);
			setFacultyListAdapter(mFacultyDataList);

			if (pd != null)
				pd.dismiss();
		}
	}

	private InputStream openHttpConnection(String urlStr) {
		InputStream in = null;
		int resCode = -1;

		try {
			URL url = new URL(urlStr);
			URLConnection urlConn = url.openConnection();

			if (!(urlConn instanceof HttpURLConnection)) {
				throw new IOException("URL is not an Http URL");
			}

			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			resCode = httpConn.getResponseCode();
			if (resCode == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return in;
	}

}
